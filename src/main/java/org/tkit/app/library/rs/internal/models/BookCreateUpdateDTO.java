package org.tkit.app.library.rs.internal.models;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.tkit.app.library.domain.models.enums.BookCategory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookCreateUpdateDTO{
    
    //@NotBlank(message="must not be blank")
    @Schema(description = "Title of the book")
    private String title;

    //@NotBlank(message="must not be blank")
    @Schema(description = "ISBN of the book")
    private String iSBN;

    //@NotNull(message = "must not be null")
    @Min(value = 1, message="must be greater than 0")
    @Schema(description = "Number of pages in the book")
    private Integer numOfPages;

    //@NotNull(message = "must not be null")
    @Schema(description = "Author of the book")
    private String authorId;

    //@NotNull(message = "must not be null")
    @Schema(description = "Category of the book")
    private BookCategory bookCategory;

    @Schema(description = "Status wheather teh book is available right now")
    private boolean isAvailable;

}
