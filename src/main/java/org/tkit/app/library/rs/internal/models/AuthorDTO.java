package org.tkit.app.library.rs.internal.models;

import javax.validation.constraints.Min;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.tkit.app.library.domain.models.enums.AuthorStatus;
import org.tkit.quarkus.rs.models.TraceableDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
//@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class AuthorDTO extends TraceableDTO{
    
    
    @Schema(description = "Name of the author")
    private String name;

  
    @Schema(description = "Last name of the author")
    private String lastName;


    @Min(value = 1, message="must be greater than or equal to 1 ")
    @Schema(description = "Age of the author")
    private Integer age;


    @Schema(description = "Current status of the author")
    private AuthorStatus authorStatus;

}
