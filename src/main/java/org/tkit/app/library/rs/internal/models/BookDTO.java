package org.tkit.app.library.rs.internal.models;

import javax.validation.constraints.Min;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.tkit.app.library.domain.models.enums.BookCategory;
import org.tkit.quarkus.rs.models.BusinessTraceableDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookDTO extends BusinessTraceableDTO{
    
    @Schema(description = "Title of the book")
    private String title;

    @Schema(description = "ISBN of the book")
    private String iSBN;

    @Min(value = 1, message="must be greater than 0")
    @Schema(description = "Number of pages in the book")
    private Integer numOfPages;

    //@Schema(description = "Author of the book")
    //private AuthorDTO authorDTO;
    @Schema(description = "Author of the book")
    private AuthorDTO authorDTO;

    @Schema(description = "Category of the book")
    private BookCategory bookCategory;

    @Schema(description = "Status wheather teh book is available right now")
    private boolean isAvailable;
}
