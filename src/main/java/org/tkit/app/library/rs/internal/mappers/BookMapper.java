package org.tkit.app.library.rs.internal.mappers;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.tkit.app.library.domain.models.Book;
import org.tkit.app.library.domain.models.criteria.BookSearchCriteria;
import org.tkit.app.library.rs.internal.models.BookCreateUpdateDTO;
import org.tkit.app.library.rs.internal.models.BookDTO;
import org.tkit.app.library.rs.internal.models.criteria.BookSearchCriteriaDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.mappers.OffsetDateTimeMapper;
import org.tkit.quarkus.rs.models.PageResultDTO;


@Mapper(componentModel = "cdi", uses = {AuthorMapper.class,OffsetDateTimeMapper.class}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface BookMapper {

    // @Mapping(target = "title")
    // @Mapping(target = "iSBN")
    // @Mapping(target = "numOfPages")
    @Mapping(target = "authorDTO",source = "author")
    // @Mapping(target = "bookCategory")
    // @Mapping(target = "available")
    BookDTO maptoDTO(Book book);

    @Mapping(target = "title")
    @Mapping(target = "ISBN")
    @Mapping(target = "numOfPages")
    @Mapping(target = "bookCategory")
    @Mapping(target = "available")
    Book maptoEntity(BookCreateUpdateDTO book);
    
    PageResultDTO<BookDTO> mapToPageResultDTO(PageResult<Book> page);

    BookSearchCriteria mapToSearchCriteria(BookSearchCriteriaDTO searchCriteria);
    
    @Mapping(target = "title")
    @Mapping(target = "ISBN")
    @Mapping(target = "numOfPages")
    @Mapping(target = "author")
    @Mapping(target = "bookCategory")
    @Mapping(target = "available")
    Book updateBookFromDTO(BookCreateUpdateDTO bookDTO,@MappingTarget Book book);
}
