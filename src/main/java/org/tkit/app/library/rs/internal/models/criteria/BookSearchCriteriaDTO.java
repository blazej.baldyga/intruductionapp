package org.tkit.app.library.rs.internal.models.criteria;
import org.tkit.app.library.domain.models.enums.BookCategory;
import lombok.Getter;
import lombok.Setter;

import javax.ws.rs.QueryParam;

@Getter
@Setter
public class BookSearchCriteriaDTO extends PageCriteriaDTO{
     
    @QueryParam("title")
    private String title;

    @QueryParam("firstName")
    private String firstName;
  
    @QueryParam("lastName")
    private String LastName;
    
    @QueryParam("bookCategory")
    private BookCategory bookCategory;

    @QueryParam("isAvailable")
    private boolean isAvailable;
}
