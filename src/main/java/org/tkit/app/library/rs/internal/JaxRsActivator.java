package org.tkit.app.library.rs.internal;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.servers.Server;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@OpenAPIDefinition(
        servers = {
                @Server(url = "http://localhost:8080/")
        },
        info = @Info(title = "my-library", description = "my library example application", version = "1.0")
)
@ApplicationPath("/")
public class JaxRsActivator extends Application {
}