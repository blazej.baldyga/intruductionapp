package org.tkit.app.library.rs.internal.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.tkit.app.library.domain.models.Author;
import org.tkit.app.library.domain.models.criteria.AuthorSearchCriteria;
import org.tkit.app.library.rs.internal.models.AuthorCreateUpdateDTO;
import org.tkit.app.library.rs.internal.models.AuthorDTO;
import org.tkit.app.library.rs.internal.models.criteria.AuthorSearchCriteriaDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.mappers.OffsetDateTimeMapper;
import org.tkit.quarkus.rs.models.PageResultDTO;

@Mapper(componentModel = "cdi", uses = {OffsetDateTimeMapper.class})
public interface AuthorMapper {


    @Mapping(target ="name")
    @Mapping(target ="lastName")
    @Mapping(target ="age")
    @Mapping(target ="authorStatus")
    AuthorDTO maptoDTO(Author author);

    @Mapping(target ="name")
    @Mapping(target ="lastName")
    @Mapping(target ="age")
    @Mapping(target ="authorStatus")
    Author maptoEntity(AuthorCreateUpdateDTO author);

    @Mapping(target ="name")
    @Mapping(target ="lastName")
    @Mapping(target ="age")
    @Mapping(target ="authorStatus")
    Author maptoEntity(AuthorDTO author);

    PageResultDTO<AuthorDTO> mapToPageResultDTO(PageResult<Author> page);

    AuthorSearchCriteria mapToSearchCriteria(AuthorSearchCriteriaDTO searchCriteria);
    
    @Mapping(target ="name")
    @Mapping(target ="lastName")
    @Mapping(target ="age")
    @Mapping(target ="authorStatus")
    Author updateAuthorFromDTO(AuthorCreateUpdateDTO authorDTO,@MappingTarget Author author);

}
