package org.tkit.app.library.rs.internal.controllers;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.tkit.app.library.domain.daos.AuthorDAO;
import org.tkit.app.library.domain.daos.BookDAO;
import org.tkit.app.library.domain.models.Author;
import org.tkit.app.library.domain.models.Book;
import org.tkit.app.library.domain.models.criteria.BookSearchCriteria;
import org.tkit.app.library.rs.internal.mappers.AuthorMapper;
import org.tkit.app.library.rs.internal.mappers.BookMapper;
import org.tkit.app.library.rs.internal.models.BookCreateUpdateDTO;
import org.tkit.app.library.rs.internal.models.BookDTO;
import org.tkit.app.library.rs.internal.models.criteria.BookSearchCriteriaDTO;
import org.tkit.app.library.rs.internal.rfc.RFCProblemDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.exceptions.RestException;
import org.tkit.quarkus.rs.models.PageResultDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import java.util.Objects;

@Path("/book")
@ApplicationScoped
@Tag(name = "Book REST")
@Produces(MediaType.APPLICATION_JSON)
public class BookRestController {
    
    @Inject
    BookDAO bookDAO;

    @Inject
    AuthorDAO authorDAO;

    @Inject
    BookMapper bookMapper;

    @Inject
    AuthorMapper authorMapper;

    @GET
    @Path("/{id}")
    @Operation(operationId = "getBookById", description = "Gets book by Id")
    @APIResponses({
        @APIResponse(responseCode = "200", description = "OK",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                                schema = @Schema(implementation = BookDTO.class))),
        @APIResponse(responseCode = "404", description = "Not found",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                                schema = @Schema(implementation = RFCProblemDTO.class))),
        @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                                schema = @Schema(implementation = RFCProblemDTO.class)))
    })
    public Response getBook(@PathParam("id") Long id){
        Book book = bookDAO.findById(id);
        if(book == null){
            throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Book not found");
        }
        return Response.status(Response.Status.OK)
                        .type(MediaType.APPLICATION_JSON_TYPE)
                        .entity(bookMapper.maptoDTO(book))
                        .build();
    }

    @GET
    @Operation(operationId = "getBooksByCriteria", description = "Gets book by criteria")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The corresponding book resources",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = PageResultDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
    })
    public Response getBookingsByCriteria(@BeanParam BookSearchCriteriaDTO criteriaDTO) {
        BookSearchCriteria criteria = bookMapper.mapToSearchCriteria(criteriaDTO);
        PageResult<Book> book = bookDAO.searchByCriteria(criteria);

        return Response.ok(bookMapper.mapToPageResultDTO(book)).build();

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Operation(operationId = "createBook", description = "Create the book")
    @APIResponses({
        @APIResponse(responseCode = "201", description = "Created book resource",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDTO.class)),
                    headers = {@Header(name = "Location",description = "URL for the cereated book resource")}),
        @APIResponse(responseCode = "400", description = "Bad request",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
        @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
    })
    public Response saveBook(@Valid BookCreateUpdateDTO bookDTO){
        Author author = authorDAO.findById(bookDTO.getAuthorId()); 
        if(Objects.nonNull(author)){  
            Book book = bookMapper.maptoEntity(bookDTO);
            setAuthor(book, author);
            if(Objects.nonNull(book)){
                if(Objects.nonNull(book.getAuthor())){
                    bookDAO.create(book);
                    return Response.status(Response.Status.CREATED)
                                .entity(bookMapper.maptoDTO(book))
                                .build();
                }
                throw new RestException(Response.Status.NO_CONTENT, Response.Status.NO_CONTENT, "No author Set to the book");
            }
            throw new RestException(Response.Status.NO_CONTENT,Response.Status.NO_CONTENT, "No content given to created book");
        }
        throw new RestException(Response.Status.NO_CONTENT,Response.Status.NO_CONTENT, "No Author given to created book");  
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Operation(operationId = "deleteBook", description = "Delete Book")
    @APIResponse(responseCode = "204", description = "Deleted Book by Id",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                        schema = @Schema(implementation = BookDTO.class)))
    @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    public Response deleteBook(@PathParam("id") Long id){
        Book book = bookDAO.findById(id);
        if(Objects.nonNull(book)){
            bookDAO.delete(book);
            return Response.status(Response.Status.NO_CONTENT)
                            .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Book not found");
    }
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @Transactional
    @Operation(operationId = "updateBook", description = "Update Book")
    @APIResponses({
        @APIResponse(responseCode = "200", description = "Updated Book resource",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDTO.class))),
        @APIResponse(responseCode = "400", description = "Invalid request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
        @APIResponse(responseCode = "404", description = "Not Found",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
        @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))})
    public Response updateBook(@PathParam("id") Long id, @Valid BookCreateUpdateDTO bookDTO){
        Book book = bookDAO.findById(id);
        if(Objects.nonNull(book)){
            Author author = authorDAO.findById(book.getAuthor().getId());
            if(Objects.nonNull(author)){
            Book updatedBook =  bookMapper.updateBookFromDTO(bookDTO, book);
            return Response.status(Response.Status.CREATED)
                            .entity(bookMapper.maptoDTO(bookDAO.update(updatedBook)))
                            .build();
        
            }
            else{
                throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Author not found");
            }
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Book not found");
    }

    private void setAuthor(Book book, Author author){
        book.setAuthor(author);
    }
}
