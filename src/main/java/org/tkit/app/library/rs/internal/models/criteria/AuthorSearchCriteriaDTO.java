package org.tkit.app.library.rs.internal.models.criteria;

import org.tkit.app.library.domain.models.enums.AuthorStatus;
import lombok.Getter;
import lombok.Setter;

import javax.ws.rs.QueryParam;

@Getter
@Setter
public class AuthorSearchCriteriaDTO extends PageCriteriaDTO{
    
    @QueryParam("name")
    private String name;

    @QueryParam("lastName")
    private String lastName;

    @QueryParam("age")
    private int age;

    @QueryParam("AuthorStatus")
    private AuthorStatus authorStatus;

}
