package org.tkit.app.library.rs.internal.models;

import javax.validation.constraints.Min;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.tkit.app.library.domain.models.enums.AuthorStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorCreateUpdateDTO{
    
    //@NotBlank(message="must not be blank")
    @Schema(description = "Name of the author")
    private String name;

    //@NotBlank(message="must not be blank")
    @Schema(description = "Last name of the author")
    private String lastName;

    //@NotNull(message = "must not be null")
    @Min(value = 1, message="must be greater than or equal to 1 ")
    @Schema(description = "Age of the author")
    private Integer age;

    //@NotNull(message = "must not be null")
    @Schema(description = "Current status of the author")
    private AuthorStatus authorStatus;
}
