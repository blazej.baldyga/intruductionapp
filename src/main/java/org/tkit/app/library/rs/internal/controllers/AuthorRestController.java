package org.tkit.app.library.rs.internal.controllers;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.tkit.app.library.domain.daos.AuthorDAO;
import org.tkit.app.library.domain.models.Author;
import org.tkit.app.library.domain.models.criteria.AuthorSearchCriteria;
import org.tkit.app.library.rs.internal.mappers.AuthorMapper;
import org.tkit.app.library.rs.internal.models.AuthorCreateUpdateDTO;
import org.tkit.app.library.rs.internal.models.AuthorDTO;
import org.tkit.app.library.rs.internal.models.criteria.AuthorSearchCriteriaDTO;
import org.tkit.app.library.rs.internal.rfc.RFCProblemDTO;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.exceptions.RestException;
import org.tkit.quarkus.rs.models.PageResultDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Path("/author")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Author REST")
public class AuthorRestController {
    
    @Inject
    AuthorDAO authorDAO;

    @Inject
    AuthorMapper authorMapper;

    @GET
    @Path("/{id}")
    @Operation(operationId = "getAuthorById", description = "Gets Author by ID")
    @APIResponses({
        @APIResponse(responseCode = "200", description = "The corresponding Author resource",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                                schema = @Schema(implementation = AuthorDTO.class))),
        @APIResponse(responseCode = "404", description = "Not found",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                                schema = @Schema(implementation = RFCProblemDTO.class))),
        @APIResponse(
            responseCode = "500", description = "Internal Server Error, please check Problem Details",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON, 
                                schema = @Schema(implementation = RFCProblemDTO.class)))})
    public Response getAuthor(@PathParam("id") String id){
        Author author = authorDAO.findById(id);
        if(author == null){
            throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Author with given id not found");
        }
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                    .entity(authorMapper.maptoDTO(author))
                    .build();
    }


    @GET
    @Operation(operationId = "getAuthorsByCriteria", description = "Get Authors by Criteria")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The corresponding Authors resources",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = PageResultDTO.class))),
            @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))})
    public Response getAuthorsByCriteria(@BeanParam AuthorSearchCriteriaDTO searchCriteriaDTO) {
        AuthorSearchCriteria searchCriteria = authorMapper.mapToSearchCriteria(searchCriteriaDTO);

        PageResult<Author> authors = authorDAO.searchByCriteria(searchCriteria);
        return Response.ok(authorMapper.mapToPageResultDTO(authors))
                       .build();

    }

    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Operation(operationId = "createAuthor", description = "Create Author")
    @APIResponses({
        @APIResponse(responseCode = "201", description = "Created Author resource",
                        content =@Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class)),
                            headers = {@Header(name = "Location", description = "URL for the created Author resource")}),
        @APIResponse(responseCode = "400", description = "Bad request",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                                schema = @Schema(implementation = RFCProblemDTO.class))),
        @APIResponse(
            responseCode = "500", description = "Internal Server Error, please check Problem Details",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON, 
                                schema = @Schema(implementation = RFCProblemDTO.class)))})
    public Response saveAuthor(@Valid AuthorCreateUpdateDTO authorCreateUpdateDTO){
        Author author = authorMapper.maptoEntity(authorCreateUpdateDTO);
        if(Objects.nonNull(author)){
            authorDAO.create(author);
            return Response.status(Response.Status.CREATED)
            .entity(authorMapper.maptoDTO(author))
            .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "No content given to created Author");
        
        
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Operation(operationId = "updateAuthor", description = "Update Author")
    @APIResponses({
        @APIResponse(responseCode = "200", description = "Updated Author resource",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDTO.class))),
        @APIResponse(responseCode = "400", description = "Invalid request",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
        @APIResponse(responseCode = "404", description = "Not Found",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class))),
        @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                        content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))})
    public Response updateAuthor(@PathParam("id") String id, @Valid AuthorCreateUpdateDTO authorDTO){
        Author author = authorDAO.findById(id);
        if(Objects.nonNull(author)){
            authorMapper.updateAuthorFromDTO(authorDTO, author);
            authorDAO.update(author);
            return Response.status(Response.Status.CREATED)
                            .entity(authorMapper.maptoDTO(authorDAO.update(author)))
                            .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Author not found");
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Operation(operationId = "deleteAuthor", description = "Delete Author")
    @APIResponse(responseCode = "204", description = "Deleted Author by Id",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                        schema = @Schema(implementation = AuthorDTO.class)))
    @APIResponse(responseCode = "404", description = "Not found",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    @APIResponse(responseCode = "500", description = "Internal Server Error, please check Problem Details",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = RFCProblemDTO.class)))
    public Response deleteAuthor(@PathParam("id") String id){
        Author author = authorDAO.findById(id);
        if(Objects.nonNull(author)){
            authorDAO.delete(author);
            return Response.status(Response.Status.NO_CONTENT)
                            .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND, "Author not found");
    }
}

