package org.tkit.app.library.domain.daos;

import org.tkit.app.library.domain.models.*;
import org.tkit.app.library.domain.models.criteria.AuthorSearchCriteria;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class AuthorDAO extends AbstractDAO<Author>{
    public enum ErrorKeys{
        ERROR_FIND_AUTHOR_BY_CRITERIA,
        ERROR_FIND_AUTHOR_SERACH_CRITERIA_REQUIERD
    }
    public PageResult<Author> searchByCriteria(AuthorSearchCriteria criteria){
        if(criteria ==null){
            throw new DAOException(ErrorKeys.ERROR_FIND_AUTHOR_SERACH_CRITERIA_REQUIERD,new NullPointerException());
        }
        try{
            CriteriaQuery<Author> cq = criteriaQuery();
            Root<Author> root = cq.from(Author.class);
            List<Predicate> predicates = new ArrayList<>();
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            if(criteria.getName() !=null && !criteria.getName().isEmpty()){
                predicates.add(cb.like(cb.lower(root.get("name")),criteria.getName().toLowerCase()+"%"));
            }
            if(criteria.getLastName() !=null && !criteria.getLastName().isEmpty()){
                predicates.add(cb.like(cb.lower(root.get("lastName")),criteria.getLastName().toLowerCase()+"%"));
            }
            if(criteria.getAge() !=0){
                predicates.add(cb.equal(root.get("age"), criteria.getAge()));
            }
            if(criteria.getAuthorStatus() !=null){
                predicates.add(cb.equal(root.get("authorStatus"),criteria.getAuthorStatus()));
            }
            if(!predicates.isEmpty()){
                cq.where(predicates.toArray(new Predicate[0]));
            }
            return createPageQuery(cq, Page.of(criteria.getPageNumber(),criteria.getPageSize())).getPageResult();
        } catch(Exception exception){
            throw new DAOException(ErrorKeys.ERROR_FIND_AUTHOR_BY_CRITERIA, exception);
        }
    }
}
