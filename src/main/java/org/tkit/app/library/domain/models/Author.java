package org.tkit.app.library.domain.models;


import lombok.Getter;
import lombok.Setter;
import org.tkit.app.library.domain.models.enums.AuthorStatus;
import org.tkit.quarkus.jpa.models.TraceableEntity;

import javax.persistence.Table;
import javax.persistence.*;


@Entity
@Getter
@Setter
@Table(name = "AUTHOR")
public class Author extends TraceableEntity{
    

    /**
     * The name of the Author
     */
    @Column(name = "NAME")
    private String name;

    /**
     * The last name of the Author
     */
    @Column(name = "LASTNAME")
    private String lastName;

    /**
     * Age of the Author
     */
    @Column(name = "AGE")
    private Integer age;

    /**
     * Description whether is still alive or not 
     */
    @Column(name = "AUTHOR_STATUS")
    private AuthorStatus authorStatus;

}
