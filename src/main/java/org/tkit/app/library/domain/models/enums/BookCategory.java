package org.tkit.app.library.domain.models.enums;

import lombok.Getter;

@Getter
public enum BookCategory {
    ACTION_AND_ADVENTURE,
    CLASSICS,
    COMIC_BOOK_OR_GRAPHIC_NOVEL,
    DETECTIVE_AND_MISTERY,
    FANTASY,
    HISTORICAL_FICTION,
    HORROR,
    LITEARY_FICTION
}
