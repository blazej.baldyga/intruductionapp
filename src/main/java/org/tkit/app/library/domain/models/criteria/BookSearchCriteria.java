package org.tkit.app.library.domain.models.criteria;

import lombok.Getter;
import lombok.Setter;
import org.tkit.app.library.domain.models.enums.BookCategory;

@Getter
@Setter
public class BookSearchCriteria {
    
    /**
     * Title of the book
     */
    private String title;

    /**
     * First name of the author
     */
    private String firstName;
    /**
     * Last name of the Author
     */
    private String LastName;
    
    /**
     * Category of the book
     */
    private BookCategory bookCategory;

    /**
     * Is the bok available for actions
     */
    private boolean isAvailable;

    /**
     * The number of page.
     */
    private Integer pageNumber;

    /**
     * The size of page.
     */
    private Integer pageSize;
}
