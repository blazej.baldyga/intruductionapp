package org.tkit.app.library.domain.models;


import lombok.Getter;
import lombok.Setter;
import org.tkit.app.library.domain.models.enums.BookCategory;
import org.tkit.quarkus.jpa.models.BusinessTraceableEntity;

import javax.persistence.Table;
import javax.persistence.*;



@Entity
@Getter
@Setter
@Table(name = "BOOK")
public class Book extends BusinessTraceableEntity {
    
    /**
     * Title of the Book
     */
    @Column(name="TITLE")
    private String title;

    /**
     * ISBN number of the Book
     */
    @Column(name = "ISBN")
    private String iSBN;

    /**
     * Number of Pages in the Book
     */
    @Column(name = "NUM_OF_PAGES")
    private Integer numOfPages;

    /**
     * Author of the Book
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTHOR_GUID")
    private org.tkit.app.library.domain.models.Author author;

    /**
     * Genre/category of the Book
     */
    @Column(name = "BOOK_CATEGORY")
    private BookCategory bookCategory;

    /**
     * is book available for booking
     */
    @Column(name = "IS_AVAILABLE")
    private boolean isAvailable;

}
