package org.tkit.app.library.domain.models.criteria;

import lombok.Getter;
import lombok.Setter;
import org.tkit.app.library.domain.models.enums.AuthorStatus;

@Getter
@Setter
public class AuthorSearchCriteria {
    
    /**
     * The name of the Author
     */
    private String name;

    /**
     * The last name of the Author
     */
    
    private String lastName;

    /**
     * Age of the Author
     */
    
    private int age;

    /**
     * Description whether is still alive or not 
     */
    
    private AuthorStatus authorStatus;

    /**
     * The number of page.
     */
    private Integer pageNumber;

    /**
     * The size of page.
     */
    private Integer pageSize;

}
