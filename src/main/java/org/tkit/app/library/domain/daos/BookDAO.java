package org.tkit.app.library.domain.daos;

import org.tkit.app.library.domain.models.*;
import org.tkit.app.library.domain.models.criteria.BookSearchCriteria;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class BookDAO extends AbstractDAO<Book>{
    
    public enum ErrorKeys{
        ERROR_FIND_BOOK_BY_CRITERIA,
        ERROR_FIND_BOOK_SERACH_CRITERIA_REQUIERD
    }

     
    public PageResult<Book> searchByCriteria(BookSearchCriteria criteria){
        if(criteria ==null){
            throw new DAOException(ErrorKeys.ERROR_FIND_BOOK_SERACH_CRITERIA_REQUIERD,new NullPointerException());
        }
        try{
            CriteriaQuery<Book> cq = criteriaQuery();
            Root<Book> root = cq.from(Book.class);
            List<Predicate> predicates = new ArrayList<>();
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            if(criteria.getTitle() !=null && !criteria.getTitle().isEmpty()){
                predicates.add(cb.like(cb.lower(root.get("title")),criteria.getTitle().toLowerCase()+"%"));
            }
            if(criteria.getFirstName() !=null && !criteria.getFirstName().isEmpty()){
                predicates.add(cb.like(cb.lower(root.get("author").get("name")),criteria.getFirstName().toLowerCase()+"%"));
            }
            if(criteria.getLastName() !=null && !criteria.getLastName().isEmpty()){
                predicates.add(cb.like(cb.lower(root.get("author").get("lastName")),criteria.getLastName().toLowerCase()+"%"));
            }
            if(criteria.getBookCategory() !=null){
                predicates.add(cb.equal(root.get("bookCategory"),criteria.getBookCategory()));
            }
            if(criteria.isAvailable()==true){
                predicates.add(cb.equal(root.get("isAvailable"),criteria.isAvailable()));
            }
            if(!predicates.isEmpty()){
                cq.where(predicates.toArray(new Predicate[0]));
            }
            return createPageQuery(cq, Page.of(criteria.getPageNumber(),criteria.getPageSize())).getPageResult();
        } catch(Exception exception){
            throw new DAOException(ErrorKeys.ERROR_FIND_BOOK_BY_CRITERIA, exception);
        }
    }
    public List<Book> getBooksForGivenAuthorAndTitle(Author author, String Title){
        CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Book> cq = cb.createQuery(Book.class);
        Root<Book> root = cq.from(Book.class);
        cq.where(
                cb.like(cb.lower(root.get("name")),author.getName()),
                cb.like(cb.lower(root.get("lastName")),author.getLastName()),
                cb.like(cb.lower(root.get("title")),Title)   
        );
        TypedQuery<Book> typedQuery = em.createQuery(cq);
        return typedQuery.getResultList();
    }
}
