package org.tkit.app.library.domain.models.enums;

import lombok.Getter;


@Getter
public enum AuthorStatus {
    ALIVE,
    DECEASED
}
