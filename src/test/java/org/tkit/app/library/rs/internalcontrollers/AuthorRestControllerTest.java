package org.tkit.app.library.rs.internalcontrollers;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.DisplayName;

import org.junit.jupiter.api.Test;
import org.tkit.app.library.domain.models.enums.AuthorStatus;
import org.tkit.app.library.rs.internal.models.AuthorCreateUpdateDTO;
import org.tkit.app.library.rs.internal.models.AuthorDTO;
import org.tkit.app.library.rs.internal.rfc.RFCProblemDTO;
import org.tkit.app.test.AbstractTest;
import org.tkit.quarkus.rs.models.PageResultDTO;
import org.tkit.quarkus.test.WithDBData;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;

@QuarkusTest
@WithDBData(value = {"librarymanagment-test-data.xls"}, deleteBeforeInsert = true, rinseAndRepeat = true)
public class AuthorRestControllerTest extends AbstractTest {
    
    
    private static final String AUTHOR_NAME = "John";
    private static final String AUTHOR_LAST_NAME = "Doe";
    private static final Integer AUTHOR_AGE = 50;
    private static final AuthorStatus A_STATUS = AuthorStatus.ALIVE;
    private static final String AUTHOR_ID = "11";

    private static final String RFC_PROBLEM_DETAIL_ID_NOT_FOUND = "Author not found";
    private static final String RFC_PROBLEM_TITLE_TECHNICAL_ERROR = "TECHNICAL ERROR";
    private static final String RFC_PROBLEM_TYPE_REST_EXCEPTION = "REST_EXCEPTION";
    private static final String RFC_PROBLEM_TYPE_VALIDATION_EXCEPTION = "VALIDATION_EXCEPTION"; 
    //private static final String RFC_PROBLEM_TYPE_DAO_EXCEPTION = "DAO_EXCEPTION";

    private static final Integer AUTHOR_AGE_LESS_THAN_ONE = 0;
    private static final String AUTHOR_ID_SEARCHED = "11";
    private static final String AUTHOR_LAST_NAME_SEARCHED = "Shakespere";
    private static final Integer AGE = 46;
    private static final int AMOUNT_OF_AUTHORS = 6;
    private static final int AMOUNT_OF_AUTHORS_WITH_AGE_46 = 1;
    //private static final int AMOUNT_OF_AUTHORS_FOUND_BY_CRITERIA_SEARCH = 2;
    private static final int AUTHOR_AGE_RANDOM  = 5661;
    private static final String AUTHOR_ID_RANDOM = "5555";
    private static final String AUTHOR_DELETED_ID = "14"; 

    @Test
    @DisplayName("Get list of all authors")
    public void testSuccessfulGetListOfAllAuthors(){
        Response response = given()
                .contentType(MediaType.APPLICATION_JSON)
                .when().get("/author");
        response.then()
                .statusCode(200);
        int amountOfAuthorsFound = response.body().as(PageResultDTO.class).getStream().size();
        assertThat(amountOfAuthorsFound).isEqualTo(AMOUNT_OF_AUTHORS);
    }

    @Test
    @DisplayName("Get empty results when criteria search for author do not match any records")
    public void testSuccesfullGetEmptyResultsIfSearchFroNonExistingAuthorCriteria(){
        Response response = given()
                    .when().get("/author?age="+AUTHOR_AGE_RANDOM);
                response.then()
                    .statusCode(200)
                    .header(CONTENT_TYPE, APPLICATION_JSON);
        int amountOfAuthorsFound = response.body().as(getAuthorDTOTypeRef()).getStream().size();
        assertThat(amountOfAuthorsFound).isZero();
    }


    @Test
    @DisplayName("get authors which age is 46")
    public void testSuccessfulGetListOfAuthorByAgeCriteria(){
        Response response = given()
                            .when().get("/author/?age="+AGE);
                response.then()
                    .statusCode(200)
                    .header(CONTENT_TYPE, APPLICATION_JSON);
        int amountOfAuthorsFound = response.body().as(getAuthorDTOTypeRef()).getStream().size();
        assertThat(amountOfAuthorsFound).isEqualTo(AMOUNT_OF_AUTHORS_WITH_AGE_46);
    }

    @Test
    @DisplayName("Get specific author by id")
    public void testSuccessfulGetAuthorById(){
        Response response = given()
                    .when().get("/author/"+AUTHOR_ID_SEARCHED);
                response.then()
                    .statusCode(200);
        AuthorDTO authorFound = response.body().as(AuthorDTO.class);
        assertThat(authorFound.getId()).isEqualTo(AUTHOR_ID_SEARCHED);
        assertThat(authorFound.getLastName()).isEqualTo(AUTHOR_LAST_NAME_SEARCHED);
    }

    @Test
    @DisplayName("Get error when specific author is not found by id")
    public void testFailGetAuthorById(){
        Response response = given()
                    .when().get("/author/"+AUTHOR_ID_RANDOM);
                    response.then()
                    .statusCode(404);
        RFCProblemDTO rfcProblemDTO = response.as(RFCProblemDTO.class);
        assertThat(rfcProblemDTO.getDetail()).isEqualTo("Author with given id not found");
        assertThat(rfcProblemDTO.getTitle()).isEqualTo(RFC_PROBLEM_TITLE_TECHNICAL_ERROR);
        assertThat(rfcProblemDTO.getType()).isEqualTo(RFC_PROBLEM_TYPE_REST_EXCEPTION);
    }

    @Test
    @DisplayName("Add Author when all received data is correct")
    public void testSuccessfulAddAuthor(){
        AuthorCreateUpdateDTO authorData = new AuthorCreateUpdateDTO();
        authorData.setName(AUTHOR_NAME);
        authorData.setLastName(AUTHOR_LAST_NAME);
        authorData.setAge(AUTHOR_AGE);
        authorData.setAuthorStatus(A_STATUS);
        Response response = given()
                    .body(authorData)
                    .header(CONTENT_TYPE, APPLICATION_JSON)
                    .header(ACCEPT, APPLICATION_JSON)
                    .when().post("/author");
        AuthorDTO savedAuthor = response.then()
                    .statusCode(201)
                    .extract()
                    .body().as(AuthorDTO.class);
        int amountOfAuthorInDB =
                    given()
                            .when().get("/author")
                            .then()
                            .statusCode(200)
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .extract()
                            .body().as(getAuthorDTOTypeRef()).getStream().size(); 
        assertThat(amountOfAuthorInDB).isEqualTo(AMOUNT_OF_AUTHORS+1);
        assertThat(savedAuthor.getId()).isNotNull();
        assertThat(savedAuthor.getName()).isEqualTo(AUTHOR_NAME);
        assertThat(savedAuthor.getLastName()).isEqualTo(AUTHOR_LAST_NAME);
        assertThat(savedAuthor.getAge()).isEqualTo(AUTHOR_AGE);
        assertThat(savedAuthor.getAuthorStatus()).isEqualTo(A_STATUS);
    }

    @Test
    @DisplayName("Get error when trying to add author which age is less than 1")
    public void testFailAddAuthorWhichAgeLessThanOne(){
        AuthorCreateUpdateDTO authorData = new AuthorCreateUpdateDTO();
        authorData.setName(AUTHOR_NAME);
        authorData.setLastName(AUTHOR_LAST_NAME);
        authorData.setAge(AUTHOR_AGE_LESS_THAN_ONE);
        authorData.setAuthorStatus(A_STATUS);
        Response response = given()
                .body(authorData)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .when().post("/author");
        response.then().statusCode(400);
        RFCProblemDTO rfcProblemDTO = response.as(RFCProblemDTO.class);
        assertThat(rfcProblemDTO.getType()).isEqualTo(RFC_PROBLEM_TYPE_VALIDATION_EXCEPTION);
        assertThat(rfcProblemDTO.getDetail()).isEqualTo("saveAuthor.authorCreateUpdateDTO.age: must be greater than or equal to 1 ");

    }

    @Test
    @DisplayName("Update author when all received data is correct")
    public void testSuccessfulUpdateAuthor(){
        AuthorCreateUpdateDTO updateAuthor = new AuthorCreateUpdateDTO();
        updateAuthor.setName(AUTHOR_NAME);
        updateAuthor.setLastName(AUTHOR_LAST_NAME);
        updateAuthor.setAge(AUTHOR_AGE);
        updateAuthor.setAuthorStatus(A_STATUS);
        Response response = given()
                .body(updateAuthor)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .when().put("/author/"+AUTHOR_ID);
        response.then()
                .statusCode(201);
        AuthorDTO author = response
                    .body()
                    .as(AuthorDTO.class);

        assertThat(author.getId()).isEqualTo(AUTHOR_ID);
        assertThat(author.getName()).isEqualTo(AUTHOR_NAME);
        assertThat(author.getLastName()).isEqualTo(AUTHOR_LAST_NAME);
        assertThat(author.getAge()).isEqualTo(AUTHOR_AGE);
        assertThat(author.getAuthorStatus()).isEqualTo(A_STATUS);
        int amountOfAuthorInDB =
                given()
                        .when().get("/author")
                        .then()
                        .statusCode(200)
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract()
                        .body().as(getAuthorDTOTypeRef()).getStream().size();
        assertThat(amountOfAuthorInDB).isEqualTo(AMOUNT_OF_AUTHORS);
    }

    @Test
    @DisplayName("Delete author when given correct id")
    public void testSuccessfulDeleteAuthorWhenGivenCorrectId(){
        Response response = given()
                    .when().delete("/author/"+AUTHOR_DELETED_ID);
        response.then()
                .statusCode(204);
        int amountOfAuthorInDB =
                    given()
                            .when().get("/author")
                            .then()
                            .statusCode(200)
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .extract()
                            .body().as(getAuthorDTOTypeRef()).getStream().size();
        assertThat(amountOfAuthorInDB).isEqualTo(AMOUNT_OF_AUTHORS-1);
    }

    @Test
    @DisplayName("Get error when trying to delete author using incorrect id")
    public void testFailDeleteAuthorUsingIncorrectId(){
        Response response = given()
                    .when().delete("/author/"+AUTHOR_ID_RANDOM);
        response.then()
                .statusCode(404);
        RFCProblemDTO rfcProblemDTO = response.as(RFCProblemDTO.class);
        assertThat(rfcProblemDTO.getType()).isEqualTo(RFC_PROBLEM_TYPE_REST_EXCEPTION);
    }


    private TypeRef<PageResultDTO<AuthorDTO>> getAuthorDTOTypeRef() {
        return new TypeRef<>() {
        };
    }
}
