package org.tkit.app.library.rs.internalcontrollers;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;


import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.tkit.app.library.domain.models.Author;
import org.tkit.app.library.domain.models.enums.AuthorStatus;
import org.tkit.app.library.domain.models.enums.BookCategory;
import org.tkit.app.library.rs.internal.models.BookCreateUpdateDTO;
import org.tkit.app.library.rs.internal.models.BookDTO;
import org.tkit.app.library.rs.internal.rfc.RFCProblemDTO;
import org.tkit.app.test.AbstractTest;
import org.tkit.quarkus.rs.models.PageResultDTO;
import org.tkit.quarkus.test.WithDBData;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;



@QuarkusTest
@WithDBData(value = {"librarymanagment-test-data.xls"}, deleteBeforeInsert = true, rinseAndRepeat = true)
public class BookRestControllerTest extends AbstractTest{
    
    private static final String BOOK_TITLE = "Apocalipse";
    private static final String BOOK_ISBN = "9465821365748";
    private static final Integer BOOK_NUM_OF_PAGES = 340;
    private static final String BOOK_AUTHOR_ID = "10";
    private static final BookCategory BOOK_CATEGORY = BookCategory.HORROR;
    private static final boolean BOOK_IS_AVAILABLE = true;
    private static Author author = new Author(); 

    private static final String RFC_PROBLEM_DETAIL_ID_NOT_FOUND = "Book not found";
    private static final String RFC_PROBLEM_TITLE_TECHNICAL_ERROR = "TECHNICAL ERROR";
    private static final String RFC_PROBLEM_TYPE_REST_EXCEPTION = "REST_EXCEPTION";
    private static final String RFC_PROBLEM_TYPE_VALIDATION_EXCEPTION = "VALIDATION_EXCEPTION"; 

    
    private static final Integer BOOK_NUM_OF_PAGES_LESS_THAN_ONE = 0;
    private static final Long BOOK_ID_SEARCHED = 20l;

    private static final int AMOUNT_OF_BOOKS = 5;
    private static final String TITLE_SEARCHED_CRITERIA = "Hamlet";
    private static final int AMOUNT_OF_BOOKS_WHICH_TITLE_HAMLET = 1;
    private static final String BOOK_TITLE_RANDOM  = "asdasd";
    private static final Long BOOK_ID_RANDOM = 5555l;
    private static final Long BOOK_DELETED_ID = 23l;
    private static final Long BOOK_ID = 20l;
    

    private static final String SEARCHED_BOOK_TITLE = "Animal Farm";
    private static final String SEARCHED_BOOK_ISBN = "3239436589756";
    private static final Integer SEARCHED_BOOK_NUM_OF_PAGES = 300;
    private static final BookCategory SEARCHED_BOOK_CATEGORY = BookCategory.LITEARY_FICTION;
    private static final boolean SEARCHED_BOOK_IS_AVAILABLE = true;

    @Test
    @DisplayName("Get list of all books")
    public void testSuccessfilGetListOfAllBooks(){
        Response response = given()
                .contentType(MediaType.APPLICATION_JSON)
                .when().get("/book");
        response.then()
                .statusCode(200);
        int amountOfBookFound = response.body().as(PageResultDTO.class).getStream().size();
        assertThat(amountOfBookFound).isEqualTo(AMOUNT_OF_BOOKS);
    }

    @Test
    @DisplayName("Get empty resutls when criteria search for book does not match any records")
    public void testSuccessfulGetEmptyResulteIfSearchForNonExistingBook(){
        Response response = given()
                    .when().get("/book/?title="+BOOK_TITLE_RANDOM);
                response.then()
                    .statusCode(200)
                    .header(CONTENT_TYPE, APPLICATION_JSON);
        int amountOfBooksFound = response.body().as(getBookDTOTypeRef()).getStream().size();
        assertThat(amountOfBooksFound).isZero();
    }

    @Test
    @DisplayName("Get books which title Hamlet")
    public void testSuccessfulGetListOfBooksByTitleCriteria(){
        Response response = given()
                        .when().get("/book/?title="+TITLE_SEARCHED_CRITERIA);
        response.then() 
                .statusCode(200)
                .header(CONTENT_TYPE, APPLICATION_JSON);
        int amountOfBooksFound = response.body().as(getBookDTOTypeRef()).getStream().size();
        assertThat(amountOfBooksFound).isEqualTo(AMOUNT_OF_BOOKS_WHICH_TITLE_HAMLET);
    }

    @Test
    @DisplayName("Get specific book by id")
    public void testSuccessfulGetBookById(){
        Response response = given()
                    .when().get("/book/"+BOOK_ID_SEARCHED);
                response.then()
                    .statusCode(200);
        BookDTO bookFound = response.body().as(BookDTO.class);
        assertThat(bookFound.getId()).isEqualTo(BOOK_ID_SEARCHED);
        assertThat(bookFound.getTitle()).isEqualTo(SEARCHED_BOOK_TITLE);
        assertThat(bookFound.getISBN()).isEqualTo(SEARCHED_BOOK_ISBN);
        assertThat(bookFound.getNumOfPages()).isEqualTo(SEARCHED_BOOK_NUM_OF_PAGES);
        assertThat(bookFound.getBookCategory()).isEqualTo(SEARCHED_BOOK_CATEGORY);
        assertThat(bookFound.isAvailable()).isEqualTo(SEARCHED_BOOK_IS_AVAILABLE);
        fillAuthor1();
        assertThat(bookFound.getAuthorDTO().getId()).isEqualTo(author.getId());
    }

    @Test
    @DisplayName("Get error when specific book is not found by id")
    public void testFailGetBookById(){
        Response response = given()
                    .when().get("/book/"+BOOK_ID_RANDOM);
                response.then()
                .statusCode(404);
        RFCProblemDTO rfcProblemDTO = response.as(RFCProblemDTO.class);
        assertThat(rfcProblemDTO.getDetail()).isEqualTo(RFC_PROBLEM_DETAIL_ID_NOT_FOUND);
        assertThat(rfcProblemDTO.getTitle()).isEqualTo(RFC_PROBLEM_TITLE_TECHNICAL_ERROR);
        assertThat(rfcProblemDTO.getType()).isEqualTo(RFC_PROBLEM_TYPE_REST_EXCEPTION);
    }

    @Test
    @DisplayName("Add book when all recieved data is correct")
    public void testSuccessfulAddBook(){
        BookCreateUpdateDTO bookData = new BookCreateUpdateDTO();
        bookData.setTitle(BOOK_TITLE);
        bookData.setISBN(BOOK_ISBN);
        bookData.setNumOfPages(BOOK_NUM_OF_PAGES);
        bookData.setBookCategory(BOOK_CATEGORY);
        bookData.setAvailable(BOOK_IS_AVAILABLE);
        fillAuthor1();
        bookData.setAuthorId(author.getId());
        Response response = given()
                    .body(bookData)
                    .header(CONTENT_TYPE, APPLICATION_JSON)
                    .header(ACCEPT, APPLICATION_JSON)
                    .when().post("/book");
        BookDTO savedBook = response.then()
                    .statusCode(201)
                    .extract()
                    .body().as(BookDTO.class);
        int amountOfBookInDB =
                given()
                        .when().get("/book")
                        .then()
                        .statusCode(200)
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract()
                        .body().as(getBookDTOTypeRef()).getStream().size();
        assertThat(amountOfBookInDB).isEqualTo(AMOUNT_OF_BOOKS+1);
        assertThat(savedBook.getId()).isNotNull();
        assertThat(savedBook.getTitle()).isEqualTo(BOOK_TITLE);
        assertThat(savedBook.getISBN()).isEqualTo(BOOK_ISBN);
        assertThat(savedBook.getNumOfPages()).isEqualTo(BOOK_NUM_OF_PAGES);
        assertThat(savedBook.getBookCategory()).isEqualTo(BOOK_CATEGORY);
        assertThat(savedBook.isAvailable()).isEqualTo(BOOK_IS_AVAILABLE);
        fillAuthor1();
        assertThat(savedBook.getAuthorDTO().getId()).isEqualTo(author.getId());
    }

    @Test
    @DisplayName("Get error when trying to add book witch number of pages less than 1")
    public void testFailAddBookWhichNumOfPagesLessThanOne(){
        BookCreateUpdateDTO bookData = new BookCreateUpdateDTO();
        bookData.setTitle(BOOK_TITLE);
        bookData.setISBN(BOOK_ISBN);
        bookData.setNumOfPages(BOOK_NUM_OF_PAGES_LESS_THAN_ONE);
        bookData.setBookCategory(BOOK_CATEGORY);
        bookData.setAvailable(BOOK_IS_AVAILABLE);
        Response response = given()
                    .body(bookData)
                    .header(CONTENT_TYPE, APPLICATION_JSON)
                    .header(ACCEPT, APPLICATION_JSON)
                    .when().post("/book");
        response.then()
                .statusCode(400);
        RFCProblemDTO rfcProblemDTO = response.as(RFCProblemDTO.class);
        assertThat(rfcProblemDTO.getType()).isEqualTo(RFC_PROBLEM_TYPE_VALIDATION_EXCEPTION);
        assertThat(rfcProblemDTO.getDetail()).isEqualTo("saveBook.bookDTO.numOfPages: must be greater than 0");
    }

    @Test
    @DisplayName("Update book when all received data is correct")
    public void testSuccessfulUpdateBook(){
        BookCreateUpdateDTO updateBook = new BookCreateUpdateDTO();
        updateBook.setTitle(BOOK_TITLE);
        updateBook.setISBN(BOOK_ISBN);
        updateBook.setNumOfPages(BOOK_NUM_OF_PAGES);
        updateBook.setBookCategory(BOOK_CATEGORY);
        updateBook.setAvailable(BOOK_IS_AVAILABLE);
        updateBook.setAuthorId(BOOK_AUTHOR_ID);
        Response response = given()
                    .body(updateBook)
                    .header(CONTENT_TYPE, APPLICATION_JSON)
                    .header(ACCEPT, APPLICATION_JSON)
                    .when().put("/book/"+BOOK_ID);
        response.then()
                .statusCode(201);
        BookDTO book = response
                    .body()
                    .as(BookDTO.class);
        
        assertThat(book.getId()).isEqualTo(BOOK_ID);
        assertThat(book.getTitle()).isEqualTo(BOOK_TITLE);
        assertThat(book.getISBN()).isEqualTo(BOOK_ISBN);
        assertThat(book.getBookCategory()).isEqualTo(BOOK_CATEGORY);
        assertThat(book.isAvailable()).isEqualTo(BOOK_IS_AVAILABLE);
        assertThat(book.getAuthorDTO().getId()).isEqualTo(author.getId());
        int amountOfBookInDB =
                given()
                        .when().get("/book")
                        .then()
                        .statusCode(200)
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract()
                        .body().as(getBookDTOTypeRef()).getStream().size();
        assertThat(amountOfBookInDB).isEqualTo(AMOUNT_OF_BOOKS);
    }

    @Test
    @DisplayName("Delete book when given correct id")
    public void testSuccessfulDeleteBookWhenGivenCorrectId(){
        Response response = given()
                .when().delete("/book/"+BOOK_DELETED_ID);
        response.then()
                .statusCode(204);
        int amountOfBooksInDB =
                    given()
                            .when().get("/book")
                            .then()
                            .statusCode(200)
                            .header(CONTENT_TYPE, APPLICATION_JSON)
                            .extract()
                            .body().as(getBookDTOTypeRef()).getStream().size();
        assertThat(amountOfBooksInDB).isEqualTo(AMOUNT_OF_BOOKS -1);
    }

    @Test
    @DisplayName("Get error when trying to delete author using incorrect id")
    public void testFailDeleteAuthorUsingIncorrectId(){
        Response response = given()
                    .when().delete("/book/"+BOOK_ID_RANDOM);
        response.then()
                .statusCode(404);
        RFCProblemDTO rfcProblemDTO = response.as(RFCProblemDTO.class);
        assertThat(rfcProblemDTO.getType()).isEqualTo(RFC_PROBLEM_TYPE_REST_EXCEPTION);
    }

    private void fillAuthor1(){
        author.setId("10");
        author.setName("George");
        author.setLastName("Orwell");
        author.setAge(46);
        author.setAuthorStatus(AuthorStatus.DECEASED);
    }

    private TypeRef<PageResultDTO<BookDTO>> getBookDTOTypeRef(){
        return new TypeRef<>(){

        };
    }

}
